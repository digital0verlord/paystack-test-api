package com.paystack.merchant.configuration;

import com.paystack.merchant.integrations.IntegrationsAPIClient;
import com.paystack.merchant.integrations.paystack.PayStackIntegrationsService;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:application.properties")
public class IntegrationsConfig implements EnvironmentAware {

    static Environment environment;

    @Override
    public void setEnvironment(Environment environment) {
        IntegrationsConfig.environment = environment;
    }

    @Bean
    PayStackIntegrationsService payStackIntegrationsService(){
        PayStackIntegrationsService payStackIntegrationsService =
                IntegrationsAPIClient.getClient(environment.getProperty("paystack.base-url"))
                        .create(PayStackIntegrationsService.class);
        return payStackIntegrationsService;
    }
}
