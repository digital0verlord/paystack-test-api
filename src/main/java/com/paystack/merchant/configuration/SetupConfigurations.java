package com.paystack.merchant.configuration;

import com.paystack.merchant.entity.config.Configurations;
import com.paystack.merchant.entity.finance.Wallet;
import com.paystack.merchant.repository.ConfigurationsRepository;
import com.paystack.merchant.repository.UserRepository;
import com.paystack.merchant.repository.WalletRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SetupConfigurations implements CommandLineRunner {

    private final UserRepository userRepository;
    private final WalletRepository walletRepository;
    private final ConfigurationsRepository configurationsRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public SetupConfigurations(UserRepository userRepository,
                               BCryptPasswordEncoder passwordEncoder,
                               WalletRepository walletRepository,
                               ConfigurationsRepository configurationsRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.walletRepository = walletRepository;
        this.configurationsRepository = configurationsRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        //create config
        createDefaultWallet();
        createDefaultConfig();
    }

    private void createDefaultWallet() {
        List<Wallet> existingWallet = walletRepository.findAll();
        if (existingWallet.isEmpty()) {

            logger.info("Setting up the superAdmin wallet");

            Wallet wallet = new Wallet();
            wallet.setCurrentBalance(0.0);
            wallet.setPreviousBalance(0.0);

            walletRepository.save(wallet);
            logger.info("SuperAdmin wallet was created successfully...");
        }

        logger.info("SuperAdmin wallet already exists...");
    }

    private void createDefaultConfig() {
        List<Configurations> existingConfig = configurationsRepository.findAll();
        if (existingConfig.isEmpty()) {

            logger.info("Setting up default Config");

            Configurations configurations = new Configurations();
            //Discount is a flat rate
            configurations.setActiveDiscount(0.0);
            configurations.setCreatorIsChargeBearer(true);
            configurationsRepository.save(configurations);
            logger.info("Default Config was created successfully...");
        }

        logger.info("Default Config already exists...");
    }

}
