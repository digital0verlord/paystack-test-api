package com.paystack.merchant.configuration;

import com.paystack.merchant.entity.config.Configurations;
import com.paystack.merchant.entity.finance.Transactions;
import com.paystack.merchant.entity.finance.Wallet;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApiConfiguration {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public Configurations configurations() {
        Configurations configurations = new Configurations();
        return configurations;
    }

    @Bean
    public Transactions transactions() {
        Transactions transactions = new Transactions();
        return transactions;
    }

    @Bean
    public Wallet wallet() {
        Wallet wallet = new Wallet();
        return wallet;
    }

}