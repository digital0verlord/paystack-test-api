
package com.paystack.merchant.integrations.paystack.dto.response.pay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@lombok.Data
public class Data {

    @SerializedName("access_code")
    private String accessCode;
    @SerializedName("authorization_url")
    private String authorizationUrl;
    @Expose
    private String reference;

}
