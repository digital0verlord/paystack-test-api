
package com.paystack.merchant.integrations.paystack.dto.response.pay;

import com.google.gson.annotations.Expose;

@lombok.Data
public class PaymentResponse {

    @Expose
    private Data data;
    @Expose
    private String message;
    @Expose
    private Boolean status;

}
