
package com.paystack.merchant.integrations.paystack.dto.response.nameinquiry;

import com.google.gson.annotations.Expose;

@lombok.Data
public class NameInquiryResponse {

    @Expose
    private Data data;
    @Expose
    private String message;
    @Expose
    private Boolean status;

}
