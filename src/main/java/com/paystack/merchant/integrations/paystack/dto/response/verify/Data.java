
package com.paystack.merchant.integrations.paystack.dto.response.verify;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@lombok.Data
public class Data {

    @Expose
    private Long amount;
    @Expose
    private Authorization authorization;
    @Expose
    private String channel;
    @SerializedName("created_at")
    private String createdAt;
    @Expose
    private String currency;
    @Expose
    private Customer customer;
    @Expose
    private String domain;
    @Expose
    private Long fees;
    @SerializedName("fees_split")
    private FeesSplit feesSplit;
    @SerializedName("gateway_response")
    private String gatewayResponse;
    @Expose
    private Long id;
    @SerializedName("ip_address")
    private String ipAddress;
    @Expose
    private Log log;
    @Expose
    private String message;
    @Expose
    private String metadata;
    @SerializedName("order_id")
    private String orderId;
    @SerializedName("paid_at")
    private String paidAt;
    @Expose
    private String plan;
    @SerializedName("plan_object")
    private PlanObject planObject;
    @Expose
    private String reference;
    @SerializedName("requested_amount")
    private Long requestedAmount;
    @Expose
    private String status;
    @Expose
    private Subaccount subaccount;
    @SerializedName("transaction_date")
    private String transactionDate;
}
