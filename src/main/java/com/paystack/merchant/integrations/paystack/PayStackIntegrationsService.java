package com.paystack.merchant.integrations.paystack;

import com.paystack.merchant.integrations.paystack.dto.request.CreateCreatorSubAccount;
import com.paystack.merchant.integrations.paystack.dto.request.PayForProduct;
import com.paystack.merchant.integrations.paystack.dto.response.create.CreateCreatorSubAccountResponse;
import com.paystack.merchant.integrations.paystack.dto.response.nameinquiry.NameInquiryResponse;
import com.paystack.merchant.integrations.paystack.dto.response.pay.PaymentResponse;
import com.paystack.merchant.integrations.paystack.dto.response.verify.VerifyPaymentResponse;
import retrofit2.Call;
import retrofit2.http.*;

public interface PayStackIntegrationsService {

    @GET("/bank/resolve")
    Call<NameInquiryResponse> doNameInquiry(@Header("Authorization") String accessToken,
                                            @Header("Content-Type") String contentType,
                                            @Query("account_number") String account_number,
                                            @Query("bank_code") String bank_code);

    @POST("/subaccount")
    Call<CreateCreatorSubAccountResponse> createSubAccountForCreator(@Header("Authorization") String accessToken,
                                                                     @Header("Content-Type") String contentType,
                                                                     @Body CreateCreatorSubAccount request);


    @POST("/transaction/initialize")
    Call<PaymentResponse> payForCreatorProduct(@Header("Authorization") String accessToken,
                                               @Header("Content-Type") String contentType,
                                               @Body PayForProduct request);

    @GET("/transaction/verify/{reference}")
    Call<VerifyPaymentResponse> verifyUserPayment(@Header("Authorization") String accessToken,
                                                  @Header("Content-Type") String contentType,
                                                  @Path("reference") String reference);


}
