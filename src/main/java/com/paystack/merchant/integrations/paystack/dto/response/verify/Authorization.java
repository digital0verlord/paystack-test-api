
package com.paystack.merchant.integrations.paystack.dto.response.verify;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class Authorization {

    @SerializedName("account_name")
    private Object accountName;
    @SerializedName("authorization_code")
    private String authorizationCode;
    @Expose
    private String bank;
    @Expose
    private String bin;
    @Expose
    private String brand;
    @SerializedName("card_type")
    private String cardType;
    @Expose
    private String channel;
    @SerializedName("country_code")
    private String countryCode;
    @SerializedName("exp_month")
    private String expMonth;
    @SerializedName("exp_year")
    private String expYear;
    @Expose
    private String last4;
    @Expose
    private Boolean reusable;
    @Expose
    private String signature;
}
