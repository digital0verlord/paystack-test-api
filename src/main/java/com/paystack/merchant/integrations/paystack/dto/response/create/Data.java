
package com.paystack.merchant.integrations.paystack.dto.response.create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@lombok.Data
public class Data {

    @SerializedName("account_number")
    private String accountNumber;
    @Expose
    private Boolean active;
    @SerializedName("business_name")
    private String businessName;
    @Expose
    private String createdAt;
    @Expose
    private String domain;
    @Expose
    private Long id;
    @Expose
    private Long integration;
    @SerializedName("is_verified")
    private Boolean isVerified;
    @Expose
    private Boolean migrate;
    @SerializedName("percentage_charge")
    private Double percentageCharge;
    @SerializedName("settlement_bank")
    private String settlementBank;
    @SerializedName("settlement_schedule")
    private String settlementSchedule;
    @SerializedName("subaccount_code")
    private String subaccountCode;
    @Expose
    private String updatedAt;
}
