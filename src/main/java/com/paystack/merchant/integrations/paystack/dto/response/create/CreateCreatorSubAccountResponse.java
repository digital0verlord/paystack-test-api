
package com.paystack.merchant.integrations.paystack.dto.response.create;

import com.google.gson.annotations.Expose;

@lombok.Data
public class CreateCreatorSubAccountResponse {

    @Expose
    private Data data;
    @Expose
    private String message;
    @Expose
    private Boolean status;

}
