
package com.paystack.merchant.integrations.paystack.dto.response.verify;

import com.google.gson.annotations.Expose;
import lombok.Data;

@Data
public class History {

    @Expose
    private String message;
    @Expose
    private Long time;
    @Expose
    private String type;
}
