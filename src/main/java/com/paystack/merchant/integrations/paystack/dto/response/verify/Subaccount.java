
package com.paystack.merchant.integrations.paystack.dto.response.verify;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class Subaccount {

    @SerializedName("account_number")
    private String accountNumber;
    @SerializedName("business_name")
    private String businessName;
    @Expose
    private String description;
    @Expose
    private Long id;
    @Expose
    private Object metadata;
    @SerializedName("percentage_charge")
    private Double percentageCharge;
    @SerializedName("primary_contact_email")
    private Object primaryContactEmail;
    @SerializedName("primary_contact_name")
    private Object primaryContactName;
    @SerializedName("primary_contact_phone")
    private Object primaryContactPhone;
    @SerializedName("settlement_bank")
    private String settlementBank;
    @SerializedName("subaccount_code")
    private String subaccountCode;
}
