
package com.paystack.merchant.integrations.paystack.dto.request;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateCreatorSubAccount {

    @SerializedName("account_number")
    private String accountNumber;
    @SerializedName("bank_code")
    private String bankCode;
    @SerializedName("business_name")
    private String businessName;
    @SerializedName("percentage_charge")
    private Double percentageCharge;

}
