
package com.paystack.merchant.integrations.paystack.dto.response.verify;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@Data
public class Log {

    @Expose
    private Long attempts;
    @Expose
    private Long errors;
    @Expose
    private List<History> history;
    @Expose
    private List<Object> input;
    @Expose
    private Boolean mobile;
    @SerializedName("start_time")
    private Long startTime;
    @Expose
    private Boolean success;
    @SerializedName("time_spent")
    private Long timeSpent;

}
