
package com.paystack.merchant.integrations.paystack.dto.response.nameinquiry;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;


@lombok.Data
public class Data {

    @SerializedName("account_name")
    private String accountName;
    @SerializedName("account_number")
    private String accountNumber;
    @SerializedName("bank_id")
    private Long bankId;
}
