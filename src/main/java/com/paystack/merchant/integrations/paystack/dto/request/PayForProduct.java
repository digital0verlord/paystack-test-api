
package com.paystack.merchant.integrations.paystack.dto.request;

import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PayForProduct {

    @Expose
    private String amount;
    @Expose
    private String bearer;
    @Expose
    private String email;
    @Expose
    private String subaccount;
}
