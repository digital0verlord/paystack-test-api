
package com.paystack.merchant.integrations.paystack.dto.response.verify;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class Customer {

    @SerializedName("customer_code")
    private String customerCode;
    @Expose
    private String email;
    @SerializedName("first_name")
    private Object firstName;
    @Expose
    private Long id;
    @SerializedName("last_name")
    private Object lastName;
    @Expose
    private Object metadata;
    @Expose
    private Object phone;
    @SerializedName("risk_action")
    private String riskAction;
}
