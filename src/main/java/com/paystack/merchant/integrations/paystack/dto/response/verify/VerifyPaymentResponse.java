
package com.paystack.merchant.integrations.paystack.dto.response.verify;

import com.google.gson.annotations.Expose;

@lombok.Data
public class VerifyPaymentResponse {

    @Expose
    private Data data;
    @Expose
    private String message;
    @Expose
    private Boolean status;

}
