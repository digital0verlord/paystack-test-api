
package com.paystack.merchant.integrations.paystack.dto.response.verify;

import com.google.gson.annotations.Expose;
import lombok.Data;

@Data
public class FeesSplit {

    @Expose
    private Long integration;
    @Expose
    private Params params;
    @Expose
    private Long paystack;
    @Expose
    private Long subaccount;

}
