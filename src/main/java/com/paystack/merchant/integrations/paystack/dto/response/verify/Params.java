
package com.paystack.merchant.integrations.paystack.dto.response.verify;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class Params {

    @Expose
    private String bearer;
    @SerializedName("percentage_charge")
    private String percentageCharge;
    @SerializedName("transaction_charge")
    private String transactionCharge;
}
