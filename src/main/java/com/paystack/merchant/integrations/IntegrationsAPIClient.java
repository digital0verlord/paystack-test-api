package com.paystack.merchant.integrations;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.Objects;

public class IntegrationsAPIClient {
    private static Retrofit retrofit = null;

    public static Retrofit getClient(String baseUrl){
        if(Objects.isNull(retrofit)){
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
