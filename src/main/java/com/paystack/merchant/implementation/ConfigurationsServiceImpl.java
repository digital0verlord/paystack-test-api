package com.paystack.merchant.implementation;

import com.paystack.merchant.dto.others.ConfigurationsDTO;
import com.paystack.merchant.dto.response.GenericResponse;
import com.paystack.merchant.entity.config.Configurations;
import com.paystack.merchant.enums.StatusCodes;
import com.paystack.merchant.repository.ConfigurationsRepository;
import com.paystack.merchant.service.ConfigurationsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConfigurationsServiceImpl implements ConfigurationsService {

    Logger logger = LoggerFactory.getLogger(ConfigurationsService.class);

    @Autowired
    private ConfigurationsRepository configurationsRepository;


    @Override
    public Configurations getConfigurations() {
        List<Configurations> configurations =  configurationsRepository.findAll();
        return configurations.get(0);
    }

    @Override
    public GenericResponse updateConfigurations(ConfigurationsDTO request) {
        List<Configurations> configurations =  configurationsRepository.findAll();

        Configurations config;
        config =  configurations.get(0);

        try {
            config.setActiveDiscount(request.getActiveDiscount());
            config.setCreatorIsChargeBearer(request.getCreatorIsChargeBearer());
            configurationsRepository.save(config);
            logger.info("Config Updated - {}", config);

            return new GenericResponse(StatusCodes.SUCCESS, "Config Updated", config);
        } catch (Exception e) {
            logger.info("Config Update Failed due to - {}", e);
            return new GenericResponse(StatusCodes.FAILED, e.toString(), null);
        }
    }
}
