package com.paystack.merchant.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.paystack.merchant.dto.response.utitites.UtilitiesResponse;
import com.paystack.merchant.service.UtilitiesService;
import com.paystack.merchant.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class UtilitiesServiceImpl implements UtilitiesService {
    Logger logger = LoggerFactory.getLogger(UtilitiesServiceImpl.class);


    @Override
    public List<UtilitiesResponse> getUserType() {
        return serializeData(Constants.userType);
    }


    @Override
    public List<UtilitiesResponse> getUserStatusTypes()  {
        return serializeData(Constants.userStatusTypes);
    }

    private List<UtilitiesResponse> serializeData(String utilData) {
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Object>> data = null;
        try {
            data = mapper.readValue(utilData, new TypeReference<List<Map<String, Object>>>() {
            });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        List<UtilitiesResponse> list = new ArrayList<>();
        for (Map map : data) {
            list.addAll(map.values());
        }
        return list;
    }
}
