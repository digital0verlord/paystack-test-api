package com.paystack.merchant.implementation;


import com.paystack.merchant.dto.others.*;
import com.paystack.merchant.dto.request.UserRequest;
import com.paystack.merchant.dto.validation.ValidationObject;
import com.paystack.merchant.entity.authentication.TokenBlacklist;
import com.paystack.merchant.entity.user.User;
import com.paystack.merchant.exceptions.CustomException;
import com.paystack.merchant.integrations.paystack.PayStackIntegrationsService;
import com.paystack.merchant.integrations.paystack.dto.request.CreateCreatorSubAccount;
import com.paystack.merchant.integrations.paystack.dto.response.create.CreateCreatorSubAccountResponse;
import com.paystack.merchant.integrations.paystack.dto.response.nameinquiry.NameInquiryResponse;
import com.paystack.merchant.repository.TokenBlacklistRepository;
import com.paystack.merchant.repository.UserRepository;
import com.paystack.merchant.security.JwtTokenProvider;
import com.paystack.merchant.service.AuthService;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Objects;

/**
 * UserService
 */
@Service
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;
    private final TokenBlacklistRepository tokenBlacklistRepository;
    private final AuthenticationManager authenticationManager;
    private final PayStackIntegrationsService payStackIntegrationsService;

    @Value("${paystack.content-type}")
    private String contentType;

    @Value("${paystack.secret}")
    private String paystackSecret;


    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    public AuthServiceImpl(UserRepository userRepository,
                           AuthenticationManager authenticationManager,
                           JwtTokenProvider jwtTokenProvider,
                           PasswordEncoder passwordEncoder,
                           TokenBlacklistRepository tokenBlacklistRepository,
                           PayStackIntegrationsService payStackIntegrationsService) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.tokenBlacklistRepository = tokenBlacklistRepository;
        this.passwordEncoder = passwordEncoder;
        this.payStackIntegrationsService = payStackIntegrationsService;
    }

    /**
     * Create new CREATOR, USER, ADMIN
     *
     * @param request
     * @return
     */
    public User createNewUser(UserRequest request) {
        User user = new User();
        if (validateSignupDetails(request).getIsValid()) {
            user.setEmail(request.getEmail());
            user.setUsername(request.getEmail());
            user.setPhoneNumber(request.getPhoneNumber());
            user.setPassword(passwordEncoder.encode(request.getPassword()));
            user.setUserType(request.getUserType());
            user.setFirstName(request.getFirstName());
            user.setLastName(request.getLastName());

            if (user.getUserType().trim().equals("CREATOR")) {
                //Do Name inquiry on account
                NameInquiryResponse nameInquiryResponse =
                        doNameInquiry(request.getBankCode(), request.getAccountNumber());
                if (nameInquiryResponse.getStatus()) {
                    //Create Creator Sub Account
                    CreateCreatorSubAccountResponse response = createSubAccountForCreator(
                            new CreateCreatorSubAccount(
                                    request.getAccountNumber(),
                                    request.getBankCode(),
                                    request.getBusinessName(),
                                    request.getPercentageCharge()
                            ));
                    if (Objects.nonNull(response) && response.getStatus()) {
                        user.setAccountNumber(request.getAccountNumber());
                        user.setBankCode(request.getBankCode());
                        user.setBusinessName(request.getBusinessName());
                        user.setPercentageCharge(request.getPercentageCharge());
                        user.setSubAccountNumber(response.getData().getSubaccountCode());
                    } else {
                        throw new CustomException("Unable to create Creator Sub Account", HttpStatus.UNPROCESSABLE_ENTITY);
                    }
                } else {
                    throw new CustomException("Unable to Validate Account Details", HttpStatus.UNPROCESSABLE_ENTITY);
                }

            }
            userRepository.save(user);
        } else {
            throw new CustomException(validateSignupDetails(request)
                    .getValidationMessage(), HttpStatus.UNPROCESSABLE_ENTITY);

        }
        return user;
    }

    @Override
    public NameInquiryResponse doNameInquiry(String bankCode, String accountNumber) {
        NameInquiryResponse response;
        try {
            Response<NameInquiryResponse> doNameInquiry
                    = payStackIntegrationsService
                    .doNameInquiry("Bearer " + paystackSecret, contentType, accountNumber, bankCode)
                    .execute();
            response = doNameInquiry.body();
        } catch (IOException e) {
            throw new CustomException(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return response;
    }


    /**
     * Make call to Paystack to create a creator wallet
     *
     * @param request
     * @return
     */
    @Override
    public CreateCreatorSubAccountResponse createSubAccountForCreator(CreateCreatorSubAccount request) {
        CreateCreatorSubAccountResponse response;
        try {
            Response<CreateCreatorSubAccountResponse> createSubAccount
                    = payStackIntegrationsService
                    .createSubAccountForCreator("Bearer " + paystackSecret, contentType, request)
                    .execute();
            response = createSubAccount.body();
        } catch (IOException e) {
            throw new CustomException(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return response;
    }


    /**
     * Validating Request Body
     *
     * @param request
     * @return
     */
    private ValidationObject validateSignupDetails(UserRequest request) {
        ValidationObject validationObject
                = new ValidationObject(false, "Failed");
        if (Objects.isNull(request.getFirstName())
                || request.getFirstName().trim().isEmpty()) {
            validationObject.setValidationMessage("First Name cannot be empty");
        } else if (Objects.isNull(request.getLastName())
                || request.getLastName().trim().isEmpty()) {
            validationObject.setValidationMessage("Last Name cannot be empty");
        } else if (Objects.isNull(request.getEmail())
                || request.getEmail().trim().isEmpty()) {
            validationObject.setValidationMessage("Email cannot be empty");
        } else if (Objects.isNull(request.getUserType())
                || request.getUserType().trim().isEmpty()) {
            validationObject.setValidationMessage("User Type cannot be empty");
        } else if (Objects.isNull(request.getPhoneNumber())
                || request.getPhoneNumber().trim().isEmpty()) {
            validationObject.setValidationMessage("Phone Number cannot be empty");
        } else if (Objects.isNull(request.getPassword())
                || request.getPassword().trim().isEmpty()) {
            validationObject.setValidationMessage("Password cannot be empty");
        } else if ((Objects.isNull(request.getAccountNumber())
                || request.getAccountNumber().trim().isEmpty())
                && request.getUserType().trim().equals("CREATOR")) {
            validationObject.setValidationMessage("Account Number cannot be empty");
        } else if ((Objects.isNull(request.getBankCode())
                || request.getBankCode().trim().isEmpty())
                && request.getUserType().trim().equals("CREATOR")) {
            validationObject.setValidationMessage("Bank Code cannot be empty");
        } else if ((Objects.isNull(request.getBusinessName())
                || request.getBusinessName().trim().isEmpty())
                && request.getUserType().trim().equals("CREATOR")) {
            validationObject.setValidationMessage("Business Name cannot be empty");
        } else if (Objects.isNull(request.getPercentageCharge())
                && request.getUserType().trim().equals("CREATOR")) {
            validationObject.setValidationMessage("Charge cannot be empty");
        } else {
            validationObject.setIsValid(true);
            validationObject.setValidationMessage("Validation Successful");
        }
        return validationObject;
    }

    /**
     * Logging in User
     *
     * @param user
     * @return
     */
    public LoginResponseDTO loginUser(LoginRequestDTO user) {
        String email = user.getEmail();
        String password = user.getPassword();

        try {
            logger.info("Authenticating user {}...", email);
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));

            User loggedInUser = userRepository.findUserByEmail(email)
                    .orElseThrow(() ->
                            new CustomException("Invalid email/password supplied...", HttpStatus.UNPROCESSABLE_ENTITY));

            String token = jwtTokenProvider.createToken(email);
            LoginResponseDTO responseDTO = new LoginResponseDTO();
            responseDTO.setToken(token);
            responseDTO.setUserId(loggedInUser.getUserId());
            responseDTO.setUserType(loggedInUser.getUserType());
            responseDTO.setFirstName(loggedInUser.getFirstName());
            responseDTO.setLastName(loggedInUser.getLastName());
            responseDTO.setEmail(loggedInUser.getEmail());
            responseDTO.setPhoneNumber(loggedInUser.getPhoneNumber());
            responseDTO.setAccountNumber(loggedInUser.getAccountNumber());
            responseDTO.setBankCode(loggedInUser.getBankCode());
            responseDTO.setBusinessName(loggedInUser.getBusinessName());
            responseDTO.setPercentageCharge(loggedInUser.getPercentageCharge());
            responseDTO.setSubAccountNumber(loggedInUser.getSubAccountNumber());

            logger.info("Responding with {}", responseDTO);
            return responseDTO;
        } catch (AuthenticationException e) {
            throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Validating User Token
     *
     * @param token
     * @return
     */
    @Override
    public TokenValidationDTO tokenValidation(String token) {
        TokenValidationDTO tvd = new TokenValidationDTO();
        Boolean status = jwtTokenProvider.validateToken(token) && !jwtTokenProvider.isTokenExpired(token);
        tvd.setTokenStatus(status);
        return tvd;
    }

    @Override
    public Boolean passwordOrPinValidation(PasswordValidationDTO dto) {

        logger.info("About validating password for {} ", dto.getUserId());
        if (Objects.isNull(dto.getPassword())) {
            throw new CustomException("Enter password both", HttpStatus.UNPROCESSABLE_ENTITY);
        }
        User user = userRepository.findByUserId(Long.parseLong(dto.getUserId())).orElseThrow(() ->
                new CustomException("UserId cannot be found", HttpStatus.NOT_FOUND)
        );
        return passwordEncoder.encode(dto.getPassword()) == user.getPassword();
    }

    /**
     * For System generated password
     */
    @Override
    public ResetPasswordResponseDTO resetPassword(ResetPasswordDTO resetPasswordDTO) {
        return resetPasswordOrPin(resetPasswordDTO, "password", 8);
    }


    private ResetPasswordResponseDTO resetPasswordOrPin(ResetPasswordDTO resetPasswordDTO, String action, Integer randomNumber) {
        User user = userRepository.findByUserId(Long.parseLong(resetPasswordDTO.getUserId())).orElseThrow(() -> new CustomException("User not found", HttpStatus.UNAUTHORIZED));
        logger.info("About resetting " + action + " for {}", resetPasswordDTO.getUserId());
        String plainPassword = RandomStringUtils.randomAlphabetic(randomNumber);

        if (action.equals("password"))
            user.setPassword(passwordEncoder.encode(plainPassword));
        else
            // user.setPin(passwordEncoder.encode(plainPassword));
            logger.info("About updating user " + action + " for {}", resetPasswordDTO.getUserId());
        userRepository.save(user);

        return new ResetPasswordResponseDTO(plainPassword, passwordEncoder.encode(plainPassword));

    }

    /**
     * For user set password
     */
    @Override
    public void changePassword(ChangePasswordDTO changePasswordDTO, String loggedInUserId) {
        User user = userRepository.findByUserId(Long.parseLong(loggedInUserId)).orElseThrow(() -> new CustomException("You are not authorized", HttpStatus.UNAUTHORIZED));
        if (passwordEncoder.encode(changePasswordDTO.getOldPassword()).equals(user.getPassword())) {
            throw new CustomException("Incorrect password", HttpStatus.UNPROCESSABLE_ENTITY);
        }
        if (!changePasswordDTO.getNewPassword().equals(changePasswordDTO.getConfirmPassword())) {
            throw new CustomException("New password and confirm password must be the same", HttpStatus.UNPROCESSABLE_ENTITY);
        }

        user.setPassword(passwordEncoder.encode(changePasswordDTO.getNewPassword()));
        logger.info("About updating user password for {}", changePasswordDTO.getUserId());
        userRepository.save(user);
    }

    /**
     * For user who has forgotten password
     */
    @Override
    public void forgotPassword(ForgotPasswordDTO forgotPasswordDTO) {
        User user = userRepository.findByUserId(Long.parseLong(forgotPasswordDTO.getUserId())).orElseThrow(() -> new CustomException("You are not authorized", HttpStatus.UNAUTHORIZED));
        if (!forgotPasswordDTO.getNewPassword().equals(forgotPasswordDTO.getConfirmPassword())) {
            throw new CustomException("New password and confirm password must be the same", HttpStatus.UNPROCESSABLE_ENTITY);
        }
        user.setPassword(passwordEncoder.encode(forgotPasswordDTO.getNewPassword()));
        logger.info("About updating user forgot password for {}", forgotPasswordDTO.getUserId());
        userRepository.save(user);
    }

    /**
     * Get User By Token
     *
     * @param token
     * @return
     */
    @Override
    public User getCurrentUser(String token) {
        return this.getAgentByUserEmail(jwtTokenProvider.getUserId(token));
    }

    @Override
    public void logout(HttpServletRequest request) {
        String token = jwtTokenProvider.resolveToken(request);
        String email = jwtTokenProvider.getUserId(token);
        TokenBlacklist tokenBlacklist = new TokenBlacklist();
        tokenBlacklist.setToken(token);
        tokenBlacklist.setEmail(email);
        tokenBlacklistRepository.save(tokenBlacklist);
    }

    /**
     * Get User By Email
     *
     * @param email
     * @return
     */
    public User getAgentByUserEmail(String email) {
        User user = userRepository.findUserByEmail(email)
                .orElseThrow(() ->
                        new CustomException("Invalid email/password supplied...", HttpStatus.UNPROCESSABLE_ENTITY));

        user.setPassword("******************");
        return user;
    }


}
