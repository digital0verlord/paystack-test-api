package com.paystack.merchant.implementation;

import com.paystack.merchant.dto.others.PayForProductRequest;
import com.paystack.merchant.dto.response.GenericResponse;
import com.paystack.merchant.dto.validation.ValidationObject;
import com.paystack.merchant.entity.config.Configurations;
import com.paystack.merchant.entity.finance.Transactions;
import com.paystack.merchant.entity.finance.Wallet;
import com.paystack.merchant.enums.StatusCodes;
import com.paystack.merchant.exceptions.CustomException;
import com.paystack.merchant.integrations.paystack.PayStackIntegrationsService;
import com.paystack.merchant.integrations.paystack.dto.request.CreateCreatorSubAccount;
import com.paystack.merchant.integrations.paystack.dto.request.PayForProduct;
import com.paystack.merchant.integrations.paystack.dto.response.create.CreateCreatorSubAccountResponse;
import com.paystack.merchant.integrations.paystack.dto.response.pay.PaymentResponse;
import com.paystack.merchant.integrations.paystack.dto.response.verify.VerifyPaymentResponse;
import com.paystack.merchant.repository.ConfigurationsRepository;
import com.paystack.merchant.repository.TransactionRepository;
import com.paystack.merchant.repository.WalletRepository;
import com.paystack.merchant.service.TransactionsService;
import com.paystack.merchant.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.IOException;
import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class TransactionsServiceImpl implements TransactionsService {

    Logger logger = LoggerFactory.getLogger(TransactionsService.class);

    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private WalletRepository walletRepository;
    @Autowired
    private ConfigurationsRepository configurationsRepository;
    @Autowired
    private PayStackIntegrationsService payStackIntegrationsService;

    @Value("${paystack.content-type}")
    private String contentType;

    @Value("${paystack.secret}")
    private String paystackSecret;


    @Override
    public Page<Transactions> findAllTransactions(Pageable pageable) {
        Page<Transactions> page = transactionRepository.findAllByOrderByTransactionDateDesc(pageable);
        logger.info("Get All Transactions ", page);
        return page;
    }

    @Override
    public Transactions findByPaymentRef(String paymentRef) {
        Transactions transactions = transactionRepository.findByPaymentRef(paymentRef)
                .orElseThrow(() -> new CustomException("Invalid transactions ref", HttpStatus.UNPROCESSABLE_ENTITY));
        logger.info("Get Transactions ", transactions);
        return transactions;
    }

    @Override
    public Page<Transactions> findTransactionsBetween(Date fromDate, Date endDate, Pageable pageable) {
        Page<Transactions> transactions = transactionRepository.findByTransactionDateBetweenOrderByTransactionDateDesc(fromDate, endDate, pageable);
        logger.info("Get Transactions in date range ", transactions);
        return transactions;
    }

    @Override
    public GenericResponse makeTransaction(PayForProductRequest request) {
        Transactions transactions = new Transactions();
        List<Wallet> walletList = walletRepository.findAll();
        List<Configurations> configurationsList = configurationsRepository.findAll();

        Wallet wallet;
        Configurations configurations;

        //Fetch Merchant Wallet Wallet
        if (!walletList.isEmpty()) {
            wallet = walletList.get(0);
        } else {
            throw new CustomException("Unable to fetch parent Wallet", HttpStatus.UNPROCESSABLE_ENTITY);
        }

        //Fetch Merchant Discount
        if (!configurationsList.isEmpty()) {
            configurations = configurationsList.get(0);
        } else {
            throw new CustomException("Unable to fetch System Configurations", HttpStatus.UNPROCESSABLE_ENTITY);
        }


        if (validatePaymentRequest(request).getIsValid()) {
            try {
                transactions.setAmount(request.getAmount() - configurations.getActiveDiscount());
                transactions.setDiscount(configurations.getActiveDiscount());
                transactions.setEmail(request.getEmail());
                transactions.setSubaccount(request.getSubaccount());
                transactions.setPhoneNumber(request.getPhoneNumber());
                transactions.setItemId(request.getItemId());
                transactions.setPaymentRef(Constants.generateSystemTranRef());
                logger.info("New Transaction Added - {}", transactions);


                //Making Payment
                PaymentResponse paymentResponse = payForCreatorProduct(
                        new PayForProduct(
                                String.valueOf(request.getAmount() - configurations.getActiveDiscount()),
                                configurations.getCreatorIsChargeBearer() ? "subaccount" : "",
                                request.getEmail(),
                                request.getSubaccount()));

                if (Objects.nonNull(paymentResponse)) {
                    transactions.setPaystackRef(paymentResponse.getData().getReference());

                    //Verifying Payment
                    VerifyPaymentResponse verifyPaymentResponse = verifyUserPayment(transactions.getPaystackRef());
                    if (Objects.nonNull(verifyPaymentResponse)) {
                        transactions.setStatus(verifyPaymentResponse.getData().getMessage());

                        //Updating Wallet Balance
                        wallet.setPreviousBalance(wallet.getCurrentBalance());
                        wallet.setCurrentBalance(wallet.getCurrentBalance()
                                + verifyPaymentResponse.getData().getFeesSplit().getIntegration());
                        walletRepository.save(wallet);
                    }
                }

                transactionRepository.save(transactions);
                return new GenericResponse(StatusCodes.SUCCESS, "Transaction Completed", transactions);
            } catch (Exception e) {
                logger.info("Transaction Add Failed due to - {}", e);
                return new GenericResponse(StatusCodes.FAILED, e.toString(), null);
            }
        } else {
            throw new CustomException(validatePaymentRequest(request)
                    .getValidationMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Validating payment request Body
     *
     * @param request
     * @return
     */
    private ValidationObject validatePaymentRequest(PayForProductRequest request) {
        ValidationObject validationObject
                = new ValidationObject(false, "Failed");
        if (Objects.isNull(request.getEmail())
                || request.getEmail().trim().isEmpty()) {
            validationObject.setValidationMessage("Email cannot be empty");
        } else if (Objects.isNull(request.getSubaccount())
                || request.getSubaccount().trim().isEmpty()) {
            validationObject.setValidationMessage("CREATOR account cannot be empty");
        } else if (Objects.isNull(request.getEmail())
                || request.getEmail().trim().isEmpty()) {
            validationObject.setValidationMessage("Email cannot be empty");
        } else if (Objects.isNull(request.getItemId())
                || request.getItemId().trim().isEmpty()) {
            validationObject.setValidationMessage("ItemId cannot be empty");
        } else if (Objects.isNull(request.getPhoneNumber())
                || request.getPhoneNumber().trim().isEmpty()) {
            validationObject.setValidationMessage("Phone Number cannot be empty");
        } else if (Objects.isNull(request.getAmount())) {
            validationObject.setValidationMessage("Charge cannot be empty");
        } else {
            validationObject.setIsValid(true);
            validationObject.setValidationMessage("Validation Successful");
        }
        return validationObject;
    }

    @Override
    public PaymentResponse payForCreatorProduct(PayForProduct request) {
        PaymentResponse response;
        try {
            Response<PaymentResponse> paymentResponseResponse
                    = payStackIntegrationsService
                    .payForCreatorProduct("Bearer " + paystackSecret, contentType, request)
                    .execute();
            response = paymentResponseResponse.body();
        } catch (IOException e) {
            throw new CustomException(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return response;
    }

    @Override
    public VerifyPaymentResponse verifyUserPayment(String paystackRef) {
        VerifyPaymentResponse response;
        try {
            Response<VerifyPaymentResponse> verifyPaymentResponseResponse
                    = payStackIntegrationsService
                    .verifyUserPayment("Bearer " + paystackSecret, contentType, paystackRef)
                    .execute();
            response = verifyPaymentResponseResponse.body();
        } catch (IOException e) {
            throw new CustomException(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return response;
    }

    @Override
    public Page<Transactions> findTransactionsByStatusBetween(Date fromDate, Date endDate, String status, Pageable pageable) {
        Page<Transactions> transactions = transactionRepository.findByTransactionDateBetweenAndStatusOrderByTransactionDateDesc(fromDate, endDate, status, pageable);
        logger.info("Get By Status Transactions in date range ", transactions);
        return transactions;
    }

    @Override
    public Page<Transactions> findTransactionsByStatus(String status, Pageable pageable) {
        Page<Transactions> transactions = transactionRepository.findByStatusOrderByTransactionDateDesc(status, pageable);
        logger.info("Get Transactions By Status ", transactions);
        return transactions;
    }


}
