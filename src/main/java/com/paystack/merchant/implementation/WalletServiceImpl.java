package com.paystack.merchant.implementation;

import com.paystack.merchant.entity.finance.Wallet;
import com.paystack.merchant.repository.WalletRepository;
import com.paystack.merchant.service.WalletService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WalletServiceImpl implements WalletService {

    Logger logger = LoggerFactory.getLogger(WalletService.class);

    @Autowired
    private WalletRepository walletRepository;

    @Override
    public Wallet getAdminWallet() {
        List<Wallet> temp = walletRepository.findAll();
        return temp.get(0);
    }

}
