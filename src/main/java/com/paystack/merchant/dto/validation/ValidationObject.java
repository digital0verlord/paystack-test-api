
package com.paystack.merchant.dto.validation;

import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ValidationObject {

    @Expose
    private Boolean isValid;
    @Expose
    private String validationMessage;
}
