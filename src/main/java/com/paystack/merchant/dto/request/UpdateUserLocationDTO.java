package com.paystack.merchant.dto.request;

import lombok.Data;


@Data
public class UpdateUserLocationDTO {
    private Double currentLat;
    private Double currentLng;

    public UpdateUserLocationDTO() {
    }
}
