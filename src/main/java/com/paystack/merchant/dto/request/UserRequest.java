package com.paystack.merchant.dto.request;

import com.google.gson.annotations.SerializedName;
import lombok.*;


@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRequest {

    private String firstName;
    private String lastName;
    private String email;
    private String userType;
    private String phoneNumber;
    private String password;
    private String accountNumber;
    private String bankCode;
    private String businessName;
    private Double percentageCharge;
}
