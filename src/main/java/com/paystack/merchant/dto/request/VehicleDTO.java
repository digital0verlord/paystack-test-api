package com.paystack.merchant.dto.request;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class VehicleDTO {
    @NotBlank(message = "Vehicle Type Cannot be Blank")
    private String vehicleType;
    @NotBlank(message = "Vehicle Make Cannot be Blank")
    private String vehicleMake;
    @NotBlank(message = "Vehicle Model Cannot be Blank")
    private String vehicleModel;
    @NotBlank(message = "Vehicle Plate Cannot be Blank")
    private String vehiclePlateNumber;
    @NotBlank(message = "Vehicle Carry Weight Cannot be Blank")
    private Double maximumCarryWeight;
    @NotBlank(message = "Vehicle Status Cannot be Blank")
    private String vehicleStatus;
}
