package com.paystack.merchant.dto.response.utitites;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class Datum {
    public String name;
    public int id;
}
