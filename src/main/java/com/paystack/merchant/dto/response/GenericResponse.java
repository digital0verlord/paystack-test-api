package com.paystack.merchant.dto.response;

import com.paystack.merchant.enums.StatusCodes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Component
public class GenericResponse {
    StatusCodes status;
    String message;
    Object body;
}
