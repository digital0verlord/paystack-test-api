package com.paystack.merchant.dto.response.utitites;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class UtilitiesResponse {
    public Datum data;
}
