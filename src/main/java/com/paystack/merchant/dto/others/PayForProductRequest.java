
package com.paystack.merchant.dto.others;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PayForProductRequest {
    private double amount;
    private String email;
    private String subaccount;
    private String itemId;
    private String phoneNumber;
}
