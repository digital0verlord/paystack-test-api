package com.paystack.merchant.dto.others;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import javax.validation.constraints.NotBlank;

@Data
public class ConfigurationsDTO {
    @NotBlank(message = "charge cannot be blank")
    @JsonIgnoreProperties
    private Boolean creatorIsChargeBearer;

    @NotBlank(message = "discount cannot be blank")
    @JsonIgnoreProperties
    private Double activeDiscount;
}
