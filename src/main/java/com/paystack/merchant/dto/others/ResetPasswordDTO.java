package com.paystack.merchant.dto.others;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResetPasswordDTO {
  @NotBlank(message = "userId should not be empty")
  private String userId;
  @NotBlank(message = "message should not blank")
  private String password;

  public ResetPasswordDTO(String password) {
    this.password = password;
  }

}
