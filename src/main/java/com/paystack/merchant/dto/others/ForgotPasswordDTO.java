package com.paystack.merchant.dto.others;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ForgotPasswordDTO {
    @NotBlank(message = "user id should not blank")
    private String userId;
    @NotBlank(message = "new password should not blank")
    private String newPassword;
    @NotBlank(message = "confirm new password should not blank")
    private String confirmPassword;
}
