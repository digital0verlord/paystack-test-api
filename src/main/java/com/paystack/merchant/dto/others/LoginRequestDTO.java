package com.paystack.merchant.dto.others;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * LoginRequestDTO
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequestDTO {
  @NotBlank(message = "Password should not be blank")
  private String password;
  @NotBlank(message = "Email should not be blank")
  private String email;
}
