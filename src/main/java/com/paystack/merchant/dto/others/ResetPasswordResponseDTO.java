package com.paystack.merchant.dto.others;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResetPasswordResponseDTO {
    private String plainPassword;
    private String encryptedPassword;
   
}