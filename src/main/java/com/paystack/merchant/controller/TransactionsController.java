package com.paystack.merchant.controller;

import com.paystack.merchant.apiresponse.ApiResponse;
import com.paystack.merchant.dto.others.PayForProductRequest;
import com.paystack.merchant.dto.response.GenericResponse;
import com.paystack.merchant.entity.finance.Transactions;
import com.paystack.merchant.service.TransactionsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/transactions")
@Validated

public class TransactionsController {
    @Autowired
    private TransactionsService transactionsService;

    Logger logger = LoggerFactory.getLogger(TransactionsController.class);

    @GetMapping("/get-all-transactions")
    public ResponseEntity<ApiResponse<Page<Transactions>>> findAllTransactions(Pageable pageable) {
        Page<Transactions> transactions = transactionsService.findAllTransactions(pageable);
        ApiResponse<Page<Transactions>> ar = new ApiResponse<>(HttpStatus.OK);
        ar.setMessage("Successful");
        ar.setData(transactions);
        return new ResponseEntity<>(ar, ar.getStatus());
    }

    @GetMapping("/get-transactions-by-ref/{paymentRef}")
    public ResponseEntity<ApiResponse<Transactions>> findByPaymentRef(@PathVariable String paymentRef) {
        Transactions transactions = transactionsService.findByPaymentRef(paymentRef);
        ApiResponse<Transactions> ar = new ApiResponse<>(HttpStatus.OK);
        ar.setMessage("Successful");
        ar.setData(transactions);
        return new ResponseEntity<>(ar, ar.getStatus());
    }

    @GetMapping("/get-transactions-by-date/{fromDate}/{endDate}")
    public ResponseEntity<ApiResponse<Page<Transactions>>> findTransactionsBetween(@PathVariable Date fromDate,
                                                                                   @PathVariable Date endDate,
                                                                                   Pageable pageable) {
        Page<Transactions> transactions = transactionsService
                .findTransactionsBetween(fromDate, endDate,pageable);
        ApiResponse<Page<Transactions>> ar = new ApiResponse<>(HttpStatus.OK);
        ar.setMessage("Successful");
        ar.setData(transactions);
        return new ResponseEntity<>(ar, ar.getStatus());
    }

    @PostMapping("/pay-for-product")
    public ResponseEntity<ApiResponse<GenericResponse>> makeTransaction(@Valid @RequestBody PayForProductRequest request) {
        GenericResponse GenericResponse = transactionsService.makeTransaction(request);
        ApiResponse<GenericResponse> ar = new ApiResponse<>(HttpStatus.OK);
        ar.setMessage("Successful");
        ar.setData(GenericResponse);
        return new ResponseEntity<>(ar, ar.getStatus());
    }

    @GetMapping("/get-transactions-by-stat-date/{status}/{fromDate}/{endDate}")
    public ResponseEntity<ApiResponse<Page<Transactions>>> findTransactionsBetween(
            @PathVariable String status,
            @PathVariable Date fromDate,
            @PathVariable Date endDate,
            Pageable pageable) {
        Page<Transactions> transactions = transactionsService
                .findTransactionsByStatusBetween(fromDate, endDate,status, pageable);
        ApiResponse<Page<Transactions>> ar = new ApiResponse<>(HttpStatus.OK);
        ar.setMessage("Successful");
        ar.setData(transactions);
        return new ResponseEntity<>(ar, ar.getStatus());
    }

    @GetMapping("/get-transactions-by-stat-date/{status}")
    public ResponseEntity<ApiResponse<Page<Transactions>>> findTransactionByStat(
            @PathVariable String status,
            Pageable pageable) {
        Page<Transactions> transactions = transactionsService
                .findTransactionsByStatus(status,pageable);
        ApiResponse<Page<Transactions>> ar = new ApiResponse<>(HttpStatus.OK);
        ar.setMessage("Successful");
        ar.setData(transactions);
        return new ResponseEntity<>(ar, ar.getStatus());
    }
}
