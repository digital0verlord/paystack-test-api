package com.paystack.merchant.controller;

import com.paystack.merchant.dto.response.utitites.UtilitiesResponse;
import com.paystack.merchant.service.UtilitiesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/utils")
@Validated

public class UtilitiesController {
    @Autowired
    private UtilitiesService utilitiesService;

    Logger logger = LoggerFactory.getLogger(UtilitiesController.class);

    @GetMapping("/get-user-type")
    public List<UtilitiesResponse> getUserType() {
        logger.info("getting user types..");

        return utilitiesService.getUserType();
    }

    @GetMapping("/get-user-status-types")
    public List<UtilitiesResponse> getUserStatusTypes() {
        logger.info("getting user status..");
        return utilitiesService.getUserStatusTypes();
    }

}
