package com.paystack.merchant.controller;

import com.paystack.merchant.apiresponse.ApiResponse;
import com.paystack.merchant.dto.others.ConfigurationsDTO;
import com.paystack.merchant.dto.response.GenericResponse;
import com.paystack.merchant.entity.config.Configurations;
import com.paystack.merchant.service.ConfigurationsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * UserController
 */
@RestController
@RequestMapping("/config")
@Validated
public class ConfigurationsController {

    Logger logger = LoggerFactory.getLogger(ConfigurationsController.class);

    @Autowired
    private ConfigurationsService configurationsService;

    @GetMapping("/get-config")
    public ResponseEntity<ApiResponse<Configurations>> getConfigurations() {
        Configurations configurations = configurationsService.getConfigurations();
        ApiResponse<Configurations> ar = new ApiResponse<>(HttpStatus.OK);
        ar.setMessage("Successful");
        ar.setData(configurations);
        return new ResponseEntity<>(ar, ar.getStatus());
    }


    @PostMapping("/update-configurations")
    public ResponseEntity<ApiResponse<GenericResponse>> updateConfigurations(@Valid @RequestBody ConfigurationsDTO request) {
        GenericResponse GenericResponse = configurationsService.updateConfigurations(request);
        ApiResponse<GenericResponse> ar = new ApiResponse<>(HttpStatus.OK);
        ar.setMessage("Successful");
        ar.setData(GenericResponse);
        return new ResponseEntity<>(ar, ar.getStatus());
    }


}