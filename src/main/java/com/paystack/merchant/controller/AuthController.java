package com.paystack.merchant.controller;

import com.paystack.merchant.apiresponse.ApiResponse;
import com.paystack.merchant.dto.others.*;
import com.paystack.merchant.dto.request.UserRequest;
import com.paystack.merchant.entity.user.User;
import com.paystack.merchant.security.JwtTokenProvider;
import com.paystack.merchant.service.AuthService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * UserController
 */
@RestController
@RequestMapping("/auth")
@Validated
@CrossOrigin
public class AuthController {

  @Autowired
  private AuthService authService;

  @Autowired
  private ModelMapper modelMapper;

  @Autowired
  private JwtTokenProvider jwtTokenProvider;


  @PostMapping("/signup")
  public ResponseEntity<ApiResponse<User>> signup(@Valid @RequestBody UserRequest user) {
    User newUser = authService.createNewUser(user);
    ApiResponse<User> ar = new ApiResponse<>(HttpStatus.CREATED);
    ar.setMessage("Successfully signed up");
    ar.setData(newUser);
    return new ResponseEntity<>(ar, HttpStatus.CREATED);
  }

  @PostMapping("/login")
  public ResponseEntity<ApiResponse<LoginResponseDTO>> login(@Valid @RequestBody LoginRequestDTO user) {
    LoginResponseDTO loginResponseDTO = authService.loginUser(user);
    ApiResponse<LoginResponseDTO> ar = new ApiResponse<>(HttpStatus.OK);
    ar.setMessage("Login Successful");
    ar.setData(loginResponseDTO);
    return new ResponseEntity<>(ar, ar.getStatus());
  }

  @GetMapping("/token/validation/{token}")
  public ResponseEntity<ApiResponse<TokenValidationDTO>> tokenValidation(@PathVariable("token") String token){
    TokenValidationDTO tokenStatus = authService.tokenValidation(token);
    ApiResponse<TokenValidationDTO> ar = new ApiResponse<>(HttpStatus.OK);
    ar.setMessage("Token validated successfully");
    ar.setData(tokenStatus);
    return new ResponseEntity<>(ar, ar.getStatus());
  }

  @GetMapping("/get-current-user/{token}")
  public ResponseEntity<ApiResponse<User>> getCurrentUser(@PathVariable("token") String userId){
    ApiResponse<User> ar = new ApiResponse<>(HttpStatus.OK);
    User user = authService.getCurrentUser(userId);
    ar.setMessage("Current user fetched");
    ar.setData(user);
    return new ResponseEntity<>(ar, ar.getStatus());
  }

  @PostMapping("/reset/password")
  public ResponseEntity<ApiResponse<ResetPasswordResponseDTO>> resetPassword(@RequestBody ResetPasswordDTO passwordDTO){
    ResetPasswordResponseDTO responseDTO = authService.resetPassword(passwordDTO);
    ApiResponse<ResetPasswordResponseDTO> ar = new ApiResponse<>(HttpStatus.OK);
    ar.setMessage("Password reset successfully");
    ar.setData(responseDTO);
    return new ResponseEntity<>(ar, ar.getStatus());
  }

  @PostMapping("/validate/password")
  public ResponseEntity<ApiResponse<?>> passwordOrPinValidation(@RequestBody PasswordValidationDTO passwordDTO){
    Boolean response = authService.passwordOrPinValidation(passwordDTO);
    ApiResponse<Boolean> ar = new ApiResponse<>(HttpStatus.OK);
    ar.setMessage("Password Validated !");
    ar.setData(response);
    return new ResponseEntity<>(ar, ar.getStatus());
  }

  @PostMapping("/forgot-password")
  public ResponseEntity<ApiResponse<?>> resetsPassword(@Valid @RequestBody ForgotPasswordDTO forgotPasswordDTO) {
    authService.forgotPassword(forgotPasswordDTO);
    ApiResponse<?> ar = new ApiResponse<>(HttpStatus.OK);
    ar.setMessage("Your password has been reset. You can go ahead and login now");
    return new ResponseEntity<>(ar, ar.getStatus());
  }

  @PostMapping("/change-password")
  public ResponseEntity<ApiResponse<?>>  changePassword(@Valid @RequestBody ChangePasswordDTO changePasswordDTO, HttpServletRequest request) {
    String loggedInUserId = jwtTokenProvider.getUserId(jwtTokenProvider.resolveToken(request));
    authService.changePassword(changePasswordDTO, loggedInUserId);
    ApiResponse<?> ar = new ApiResponse<>(HttpStatus.OK);
    ar.setMessage("Your password has been changed successfully.");
    return new ResponseEntity<>(ar, ar.getStatus());
  }

  @PostMapping("logout")
  public ResponseEntity<ApiResponse<?>> logout(HttpServletRequest request) {
    authService.logout(request);
    ApiResponse<?> response = new ApiResponse<>(HttpStatus.OK);
    response.setMessage("Logout Successful");
    return new ResponseEntity<>(response, response.getStatus());
  }
}