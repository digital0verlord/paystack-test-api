package com.paystack.merchant.controller;

import com.paystack.merchant.entity.finance.Wallet;
import com.paystack.merchant.service.WalletService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/wallet")
@Validated

public class WalletController {
    @Autowired
    private WalletService walletService;

    Logger logger = LoggerFactory.getLogger(WalletController.class);

    @GetMapping("/get-admin-wallet")
    public Wallet walletService() {
        logger.info("getting admin wallet");
        return walletService.getAdminWallet();
    }

}
