package com.paystack.merchant.security;

import com.paystack.merchant.dto.others.MyUser;
import com.paystack.merchant.entity.user.User;
import com.paystack.merchant.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


/**
 * MyUserDetails
 */
@Service
public class MyUserDetails implements UserDetailsService {

  @Autowired
  private UserRepository userRepository;

  private UserDetails GetDetails(String userId, String password) {
    return org.springframework.security.core.userdetails.User
            .withUsername(userId)
            .password(password)
            .accountExpired(false).accountLocked(false)
            .credentialsExpired(false)
            .disabled(false)
            .build();
  }


  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    User user = userRepository.findUserByEmail(email).orElseThrow(() -> new  UsernameNotFoundException("User with '" + email + "' not found"));
    UserDetails ud = new MyUser(email, user.getPassword());
    return ud;
  }

}