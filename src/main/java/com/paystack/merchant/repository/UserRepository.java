package com.paystack.merchant.repository;

import com.paystack.merchant.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer>{
  Optional<User> findByUserId(Long userId);
  Optional<User> findUserByEmail(String email);
  List<User> findByUserType(String userType);
  boolean existsByUserId(String userId);
}