package com.paystack.merchant.repository;

import com.paystack.merchant.entity.config.Configurations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigurationsRepository extends JpaRepository<Configurations, Long> {

}
