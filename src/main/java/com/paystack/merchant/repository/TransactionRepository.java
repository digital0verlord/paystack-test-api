package com.paystack.merchant.repository;

import com.paystack.merchant.entity.finance.Transactions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.Optional;

public interface TransactionRepository extends JpaRepository<Transactions, Long>{

  Optional<Transactions> findByPaymentRef(String paymentRef);

  Page<Transactions> findByTransactionDateBetweenOrderByTransactionDateDesc(Date from, Date to, Pageable pageable);

  Page<Transactions> findAllByOrderByTransactionDateDesc(Pageable pageable);

  Page<Transactions> findByStatusOrderByTransactionDateDesc(String status, Pageable pageable);

  Page<Transactions> findByTransactionDateBetweenAndStatusOrderByTransactionDateDesc(Date from, Date to,String status, Pageable pageable);
}