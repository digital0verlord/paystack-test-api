package com.paystack.merchant.repository;

import com.paystack.merchant.entity.authentication.TokenBlacklist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TokenBlacklistRepository extends JpaRepository<TokenBlacklist, Integer> {
  boolean existsByToken(String token);
}
