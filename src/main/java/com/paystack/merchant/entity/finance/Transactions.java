package com.paystack.merchant.entity.finance;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.paystack.merchant.entity.authentication.AuditModel;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Table(name = "transactions")
@Entity
@Data
public class Transactions extends AuditModel {
    @Id
    @Column(unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "paymentRef")
    @Column(unique = true)
    @JsonIgnoreProperties
    private String paymentRef;

    @NotBlank(message = "status")
    @JsonIgnoreProperties
    private String status;

    @NotBlank(message = "email")
    @JsonIgnoreProperties
    private String email;

    @NotBlank(message = "subaccount")
    @JsonIgnoreProperties
    private String subaccount;

    @NotBlank(message = "itemId")
    @JsonIgnoreProperties
    private String itemId;

    @NotBlank(message = "phoneNumber")
    @JsonIgnoreProperties
    private String phoneNumber;

    @NotBlank(message = "amount")
    @JsonIgnoreProperties
    private Double amount;

    @NotBlank(message = "discount")
    @JsonIgnoreProperties
    private Double discount;

    @NotBlank(message = "paystackRef")
    @Column(unique = true)
    @JsonIgnoreProperties
    private String paystackRef;

    @CreatedDate
    @Column(name = "transactionDate")
    private Date transactionDate;
}
