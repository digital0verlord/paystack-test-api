package com.paystack.merchant.entity.finance;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.paystack.merchant.entity.authentication.AuditModel;
import lombok.Data;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Table(name = "wallet")
@Entity
@Data
public class Wallet extends AuditModel {
    @Id
    @Column(unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "previousBalance")
    @JsonIgnoreProperties
    private Double previousBalance;

    @NotBlank(message = "currentBalance")
    @JsonIgnoreProperties
    private Double currentBalance;

    @UpdateTimestamp
    @Column(name = "lastUpdated")
    private Date lastUpdated;
}
