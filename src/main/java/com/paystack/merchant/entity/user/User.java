package com.paystack.merchant.entity.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.paystack.merchant.entity.authentication.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class User extends AuditModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;

    @NotBlank(message = "email should not be blank")
    @JsonIgnoreProperties
    private String email;

    @NotBlank(message = "username should not be blank")
    @JsonIgnoreProperties
    private String username;

    @NotBlank(message = "phone number should not be blank")
    @JsonIgnoreProperties
    private String phoneNumber;

    @NotBlank(message = "Password should not be blank")
    @JsonIgnoreProperties
    private String password;

    @NotBlank(message = "UserType should not be blank")
    private String userType;

    @NotBlank(message = "Firstname should not be blank")
    private String firstName;

    @NotBlank(message = "lastName should not be blank")
    private String lastName;

    @NotBlank(message = "Account Number should not be blank")
    private String accountNumber;

    @NotBlank(message = "Bank Code should not be blank")
    private String bankCode;

    @NotBlank(message = "Business Name should not be blank")
    private String businessName;

    @NotBlank(message = "Sub Account Number should not be blank")
    private String subAccountNumber;

    @NotBlank(message = "Percentage Charge should not be blank")
    private Double percentageCharge;
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        return userId != null && userId.equals(((User) o).getUserId());
    }
 
    @Override
    public int hashCode() {
        return 31;
    }

}