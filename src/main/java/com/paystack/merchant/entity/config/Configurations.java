package com.paystack.merchant.entity.config;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.paystack.merchant.entity.authentication.AuditModel;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Table(name = "configurations")
@Entity
@Data
public class Configurations extends AuditModel {
    @Id
    @Column(unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "charge cannot be blank")
    @JsonIgnoreProperties
    private Boolean creatorIsChargeBearer;

    @NotBlank(message = "discount cannot be blank")
    @JsonIgnoreProperties
    private Double activeDiscount;
}
