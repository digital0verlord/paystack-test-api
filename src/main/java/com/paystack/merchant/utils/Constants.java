package com.paystack.merchant.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class Constants {
    static Logger logger = LoggerFactory.getLogger(Constants.class);

    public static String userType =
            "[\n" +
                    "    {\n" +
                    "    \"level\": {\n" +
                    "      \"name\":\"ADMIN\",\n" +
                    "      \"id\":1\n" +
                    "    }\n" +
                    "  },\n" +
                    "  {\n" +
                    "    \"level\": {\n" +
                    "      \"name\":\"CREATOR\",\n" +
                    "      \"id\":2\n" +
                    "    }\n" +
                    "  },\n" +
                    "  {\n" +
                    "    \"level\": {\n" +
                    "      \"name\":\"USER\",\n" +
                    "      \"id\":3\n" +
                    "    }\n" +
                    "  }\n" +
                    "\n" +
                    "]";



    public static String userStatusTypes =
            "[\n" +
                    "    {\n" +
                    "    \"level\": {\n" +
                    "      \"name\":\"Online\",\n" +
                    "      \"id\":1\n" +
                    "    }\n" +
                    "  },\n" +
                    "  {\n" +
                    "    \"level\": {\n" +
                    "      \"name\":\"Offline\",\n" +
                    "      \"id\":2\n" +
                    "    }\n" +
                    "  }\n" +
                    "\n" +
                    "]";


    public static String generateSystemTranRef(){
        return UUID.randomUUID().toString();
    }

}
