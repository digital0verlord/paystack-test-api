package com.paystack.merchant.service;


import com.paystack.merchant.dto.response.utitites.UtilitiesResponse;

import java.util.List;

public interface UtilitiesService {

    List<UtilitiesResponse> getUserType();
    List<UtilitiesResponse> getUserStatusTypes();
}
