package com.paystack.merchant.service;

import com.paystack.merchant.dto.others.*;
import com.paystack.merchant.dto.request.UserRequest;
import com.paystack.merchant.entity.user.User;
import com.paystack.merchant.integrations.paystack.dto.request.CreateCreatorSubAccount;
import com.paystack.merchant.integrations.paystack.dto.response.create.CreateCreatorSubAccountResponse;
import com.paystack.merchant.integrations.paystack.dto.response.nameinquiry.NameInquiryResponse;

import javax.servlet.http.HttpServletRequest;

/**
 * AuthService
 */
public interface AuthService {

  User createNewUser(UserRequest user);

  void logout(HttpServletRequest request);

  LoginResponseDTO loginUser(LoginRequestDTO user);

  TokenValidationDTO tokenValidation(String token);

  Boolean passwordOrPinValidation(PasswordValidationDTO dto);

  ResetPasswordResponseDTO resetPassword(ResetPasswordDTO resetPasswordDTO);

  void changePassword(ChangePasswordDTO changePasswordDTO, String loggedInUserId);

  void forgotPassword(ForgotPasswordDTO forgotPasswordDTO);

  User getCurrentUser(String userId);

  User getAgentByUserEmail(String email);

  CreateCreatorSubAccountResponse createSubAccountForCreator(CreateCreatorSubAccount request);

  NameInquiryResponse doNameInquiry(String bankCode, String accountNumber);
}
