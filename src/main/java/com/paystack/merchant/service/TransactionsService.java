package com.paystack.merchant.service;

import com.paystack.merchant.dto.others.PayForProductRequest;
import com.paystack.merchant.dto.response.GenericResponse;
import com.paystack.merchant.entity.finance.Transactions;
import com.paystack.merchant.integrations.paystack.dto.request.CreateCreatorSubAccount;
import com.paystack.merchant.integrations.paystack.dto.request.PayForProduct;
import com.paystack.merchant.integrations.paystack.dto.response.create.CreateCreatorSubAccountResponse;
import com.paystack.merchant.integrations.paystack.dto.response.pay.PaymentResponse;
import com.paystack.merchant.integrations.paystack.dto.response.verify.VerifyPaymentResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.Date;


public interface TransactionsService {

    Page<Transactions> findAllTransactions(Pageable pageable);

    Transactions findByPaymentRef(String paymentRef);

    Page<Transactions> findTransactionsBetween(Date fromDate, Date endDate, Pageable pageable);

    GenericResponse makeTransaction(PayForProductRequest request);

    Page<Transactions> findTransactionsByStatusBetween(Date fromDate, Date endDate, String status, Pageable pageable);

    Page<Transactions> findTransactionsByStatus(String status, Pageable pageable);

    PaymentResponse payForCreatorProduct(PayForProduct request);

    VerifyPaymentResponse verifyUserPayment(String reference);

}
