package com.paystack.merchant.service;

import com.paystack.merchant.dto.others.ConfigurationsDTO;
import com.paystack.merchant.dto.response.GenericResponse;
import com.paystack.merchant.entity.config.Configurations;


public interface ConfigurationsService {

    Configurations getConfigurations();

    GenericResponse updateConfigurations(ConfigurationsDTO request);
}
