package com.paystack.merchant.service;

import com.paystack.merchant.entity.finance.Wallet;


public interface WalletService {

    Wallet getAdminWallet();
}
