FROM maven:3.6.0-jdk-11-slim AS build

COPY src /app/src

WORKDIR /app

COPY pom.xml /app

RUN mvn clean install -DskipTests

RUN ls -lh /app/target

#
# Package stage
#

FROM openjdk:11-jre-slim

WORKDIR /app

# COPY config/configs-dev.properties config/

COPY --from=build /app/target/auth_service-0.0.1-SNAPSHOT.jar auth_service-0.0.1-SNAPSHOT.jar

EXPOSE 7090

# ENTRYPOINT ["java","-jar","auth_service-0.0.1-SNAPSHOT.jar", "-Dspring.profiles.active=k8s"]

ENTRYPOINT ["java","-jar","auth_service-0.0.1-SNAPSHOT.jar"]
