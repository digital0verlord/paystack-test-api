**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

---

## Project Steps

Use these steps to build project.

1. Clone project in any IDE of your choice.
2. Create DB Schema paystack_test in any MySQL Supported DB Server of your choice.
3. Update connection String in the application.development.properties file.
4. Update paystack.secret & paystack.public keys to you correct Paystack Keys
5. Run Maven Clean Install to build dependencies

---

## Project Tips

Use these tips to test project.

1. Use /utils/get-user-type to get User types.
2. Use /auth/signup to signup.
3. Use /auth/login to login.
4. Use /config/update-configurations to update configurations
5. Use /transactions/pay-for-product to make payment
6. Use /wallet/get-admin-wallet get wallet

---

## Paystack APIS Used

These Paystack APIs were used in project.

1. Paystack Name Inquiry.
2. Paystack Create SubAccount.
3. Paystack Initialize Transaction API.
4. Paystack Transaction Verification API

---

## Project Info

Additional project Info.

1. Base Url: https://paystack-test-api.herokuapp.com/ 
2. Swagger Doc : https://paystack-test-api.herokuapp.com/swagger-ui.html
3. Deployment Environment: Heroku
4. Database Type: MySQL

---

## Sample Signup Request

{
  "accountNumber": "3071985342",
  "bankCode": "011",
  "businessName": "Ade And Sons",
  "email": "ibikunle18@gmail.com",
  "firstName": "Ibikunle",
  "lastName": "Adeoluwa",
  "password": "Asendo316!",
  "percentageCharge": 50,
  "phoneNumber": "08123418017",
  "userType": "CREATOR"
}

---

## Sample Payment Request

{
  "amount": 1000,
  "email": "ibikunle18@gmail.com",
  "itemId": "FREESUB001",
  "phoneNumber": "08123418017",
  "subaccount": "ACCT_jtqu7wlyn7wx20r"
}

---

## Sample Login Request

{
  "email": "ibikunle18@gmail.com",
  "password": "Asendo316!"
}
